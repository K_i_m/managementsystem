<?php 
  include 'config.php';

  $user_id = $_SESSION['user_id'];
  $fname = $_SESSION['f_name'];
  $lname = $_SESSION['l_name'];

?>
<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fas fa-user"></i> <?php echo ucfirst($fname)." ".ucfirst($lname); ?>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">TEST</span>
        </div>
      </li>
    </ul>
  </nav>
<!-- /.navbar -->