<script type="text/javascript">
function success_add(){
	const Toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 3000,
	ProgressSteps: 'progress-step',
	onOpen: (toast) => {
	  toast.addEventListener('mouseenter', Swal.stopTimer)
	  toast.addEventListener('mouseleave', Swal.resumeTimer)
	}
	})

	Toast.fire({
	type: 'success',
	title: 'Add successfully'
	})
}

function success_update(){
	const Toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 3000,
	ProgressSteps: 'progress-step',
	onOpen: (toast) => {
	  toast.addEventListener('mouseenter', Swal.stopTimer)
	  toast.addEventListener('mouseleave', Swal.resumeTimer)
	}
	})

	Toast.fire({
	type: 'success',
	title: 'Update successfully'
	})
}

function success_delete(){
	const Toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 3000,
	ProgressSteps: 'progress-step',
	onOpen: (toast) => {
	  toast.addEventListener('mouseenter', Swal.stopTimer)
	  toast.addEventListener('mouseleave', Swal.resumeTimer)
	}
	})

	Toast.fire({
	type: 'success',
	title: 'Delete successfully'
	})
}

function error_code(){
	const Toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 3000,
	ProgressSteps: 'progress-step',
	onOpen: (toast) => {
	  toast.addEventListener('mouseenter', Swal.stopTimer)
	  toast.addEventListener('mouseleave', Swal.resumeTimer)
	}
	})

	Toast.fire({
	type: 'error',
	title: 'Opps, Something is wrong !'
	})
}

function warningAlert(){
	const Toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 3000,
	ProgressSteps: 'progress-step',
	onOpen: (toast) => {
	  toast.addEventListener('mouseenter', Swal.stopTimer)
	  toast.addEventListener('mouseleave', Swal.resumeTimer)
	}
	})

	Toast.fire({
	type: 'warning',
	title: 'Opps, No selected items !'
	})
}

function checkAll(ele , classes='') {
	if(classes == ''){
	  var checkboxes = document.getElementsByTagName('input');
	}else{
	  var checkboxes = document.getElementsByClassName(classes);
	}
	if (ele.checked) {
	  for (var i = 0; i < checkboxes.length; i++) {
	    if (checkboxes[i].type == 'checkbox') {
	      checkboxes[i].checked = true;
	      }
	    }
	} else {
	  for (var i = 0; i < checkboxes.length; i++) 
	  {
	    //console.log(i)
	    if (checkboxes[i].type == 'checkbox') {
	      checkboxes[i].checked = false;
	    }
	  }
	}
}
</script>