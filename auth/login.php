<?php 
  include '../core/config.php';

  if(isset($_SESSION['user_id'])){
    header("Location: ../index.php");
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <link rel="stylesheet" href="../plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <link rel="stylesheet" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="../plugins/jqvmap/jqvmap.min.css">
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <script src="../plugins/jquery/jquery.js" type="text/javascript"></script>
  <script src="../plugins/datatables/jquery.dataTables.js"></script>
  <script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
  <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
  <script src="../plugins/toastr/toastr.min.js" type="text/javascript"></script>
  <script src="../dist/js/adminlte.js"></script>
</head>
<body>
  <div class="col-md-6" style="margin-left: 25%;margin-top: 5%;">
    <div class="box-header with-border">
      <h3 class="box-title">Login Form</h3>
    </div>
   <!--  <form class="form-horizontal" method="POST" id="frm_login"> -->
      <div class="box-body">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="username" id="username" placeholder="Username">
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
              <label>
                <input type="checkbox"> Remember me
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-info pull-right" id="btn_submit" onclick="check_login()"><i class="fa fa-check-circle"></i> Login</button>
      </div>
   <!--  </form> -->
  </div>
<script type="text/javascript">

function check_login(){

var username = $("#username").val();
var password = $("#password").val();

$("#btn_submit").prop('disabled', true);
$("#btn_submit").html('<i class="fa fa-spin fa-spinner"></i> Loading...');

  if(username == "" || password == ""){
    alert("Please input fields !");
    $("#btn_submit").prop('disabled', false);
    $("#btn_submit").html('<i class="fa fa-check-circle"></i> Login');
  }else{
    $.ajax({
      type:"POST",
      url:"../ajax/check_user_login.php",
      data:{
          username:username,
          password:password
      },
      success:function(data){
        //alert(data);
        if(data == 1){
          setTimeout(function(){
            window.location = "../index.php?page=dashboard";
          }, 1000);
        }else{
          alert("Invalid account !");
        }
         $("#btn_submit").prop('disabled', false);
         $("#btn_submit").html('<i class="fa fa-check-circle"></i> Login');
      }
    });
  }
}
</script>  
</body>
</html>