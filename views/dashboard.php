<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Users
    </h1>
    <!-- <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol> -->
  </section>
  <br>
  <section class="content-header">
    <button type="button" class="btn btn-primary float-right" onclick="add_modal()"><span class="fa fa-plus"></span> Add</button>
  </section>
  <br><br>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Table</h3>
          </div>
          <div class="card-body">
            <table id="table_users" class="table table-bordered table-hover" style="width: 100%;">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead> 
              <tbody>
              </tbody> 
            </table> 
          </div>    
        </div>
      </div>
    </div>
    <?php require 'modals/add_user_modal.php'; ?>
    <?php require 'modals/update_user_modal.php'; ?>
  </section>
<!-- /.content-wrapper -->
<script type="text/javascript">
$(document).ready(function(){
  get_users_data();
});

function delete_user(id){

  var confirm_delete = confirm("Are you sure to delete ?");

  if(confirm_delete == true){
     $.ajax({
      type:"POST",
      url:"ajax/delete_users.php",
      data:{
          user_id:id
      },
      success:function(data){
        if(data == 1){
          success_delete();
          get_users_data();
        }else{
          error_code();
        }
      }
  });
    return true;
  }else{

  }

}

function modal_update(id){
   $("#hidden_user_id").val(id);
   $.post("ajax/previewUser.php", {
      user_id:id
    },
    function (data, status) {
        var o = JSON.parse(data);
        $("#up_fname").val(o.f_name);
        $("#up_mname").val(o.m_name);
        $("#up_lname").val(o.l_name);
        $("#up_username").val(o.username);
        $("#up_password").val(o.password);
    }
    );
    $("#update-user-modal").modal('show');
 
}

function add_modal(){
  $("#user-modal").modal('show');
  //alert("test");
}

function delete_(){
  error_code();
}

$("#frm_add_user").submit(function(e){
 e.preventDefault();

 $.ajax({
  type:"POST",
  url:"ajax/add_users.php",
  data:$(this).serialize(),
  success:function(data){
    if(data == 1){
     success_add();
      $("#user-modal").modal('hide');
      get_users_data();

      $("#frm_add_user")[0].reset();
    }else{
      warning();
    }
  }

 });

});

$("#frm_update_user").submit(function(e){
e.preventDefault();

  $.ajax({
      type:"POST",
      url:"ajax/update_users.php",
      data:$(this).serialize(),
      success:function(data){
        //alert(data);
        if(data == 1){
          $("#update-user-modal").modal('hide');
          $('#table_users').DataTable().destroy();
          get_users_data();
          success_update();
        }else if(data == 2){
          alert("Username Already Exist !")
        }else{
          error_code();
        }
      }
  });
});

function get_users_data(){
  $('#table_users').DataTable().destroy();
  $('#table_users').dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/users_data.php",
      "dataSrc":"data",
      },
      "columns":[
        {
          "data":"count"
        },
        {
          "data":"name"
        },
        {
          "data":"username"
        },
        {
          "data":"password"
        },
        {
          "data":"user_status"
        },
        {
          "mRender":function(data, type, row){
            return "<center>"+
              "<button type='button' class='btn btn-info btn-xs' onclick='modal_update("+row.id+")'><span class='fa fa-edit'></span></button>"+

              "<button type='button' class='btn btn-danger btn-xs' onclick='delete_user("+row.id+")' style='marging-left:5px;'><span class='fa fa-trash'></span></button>";

             "</center>";
          }
        }
      ]
  });
}
</script>