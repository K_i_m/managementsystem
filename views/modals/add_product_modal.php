<div class="modal fade" id="add-product-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><strong>All <span style="color:red;">*</span> are required to fill up</strong></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
       <form method="POST" id="frm_add_product"> 
       <div class="form-group">
          <label>Product Category</label><strong><span style="color:red;"> * </span></strong>
          <input type="text" name="product_name" class="form-control" placeholder="Enter ..." autocomplete="off" required>
        </div>
        <div class="modal-footer">
          <div class="btn-group">  
            <button type="submit" class="btn btn-primary" id="btn_add_categ"><span class="fa fa-check-circle"></span> Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
          </div>
         </div>
        </form> 
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->