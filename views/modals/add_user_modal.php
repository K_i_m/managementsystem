<div class="modal fade" id="user-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><strong>All <span style="color:red;">*</span> are required to fill up</strong></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
       <form method="POST" id="frm_add_user"> 
       <div class="form-group">
          <label>First Name</label><strong><span style="color:red;"> * </span></strong>
          <input type="text" name="fname" class="form-control" placeholder="Enter ..." autocomplete="off" required>
          <br>
          <label>Middle Name</label><strong><span style="color:red;"> * </span></strong>
          <input type="text" name="mname" class="form-control" placeholder="Enter ..." autocomplete="off" required>
          <br>
          <label>Last Name</label><strong><span style="color:red;"> * </span></strong>
          <input type="text" name="lname" class="form-control" placeholder="Enter ..." autocomplete="off" required>
          <br>
          <label>Username</label><strong><span style="color:red;"> * </span></strong>
          <input type="text" name="username" class="form-control" placeholder="Enter ..." autocomplete="off" required>
          <br>
          <label>Password</label><strong><span style="color:red;"> * </span></strong>
          <input type="password" name="password"class="form-control" placeholder="Enter ..." autocomplete="off" required>
        </div>
         <div class="modal-footer">
            <div class="btn-group">
            <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
          </div>
         </div>
        </form> 
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->