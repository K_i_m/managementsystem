<div class="modal fade" id="update-user-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><strong>All <span style="color:red;">*</span> are required to fill up</strong></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
       <form method="POST" id="frm_update_user"> 
       <div class="form-group">
          <input type="hidden" name="hidden_user_id" id="hidden_user_id">
          <label>First Name</label><strong><span style="color:red;"> * </span></strong>
          <input type="text" name="up_fname" id="up_fname" class="form-control" placeholder="Enter ..." autocomplete="off" required>
          <br>
          <label>Middle Name</label><strong><span style="color:red;"> * </span></strong>
          <input type="text" name="up_mname" id="up_mname" class="form-control" placeholder="Enter ..." autocomplete="off" required>
          <br>
          <label>Last Name</label><strong><span style="color:red;"> * </span></strong>
          <input type="text" name="up_lname" id="up_lname" class="form-control" placeholder="Enter ..." autocomplete="off" required>
          <br>
          <label>Username</label><strong><span style="color:red;"> * </span></strong>
          <input type="text" name="up_username" id="up_username" class="form-control" placeholder="Enter ..." autocomplete="off" required>
          <br>
          <label>Password</label><strong><span style="color:red;"> * </span></strong>
          <input type="password" name="up_password" id="up_password" class="form-control" placeholder="Enter ..." autocomplete="off" required>
        </div>
         <div class="modal-footer">
          <div class="btn-group">
            <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Save Changes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
          </div>
         </div>
        </form> 
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->