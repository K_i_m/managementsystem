<div class="modal fade" id="update-product-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><strong>All <span style="color:red;">*</span> are required to fill up</strong></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
       <form method="POST" id="frm_update_product"> 
       <div class="form-group">
          <label>Product Name</label><strong><span style="color:red;"> * </span></strong>
          <input type="text" name="update_prod_name" id="up_fname" class="form-control" placeholder="Enter ..." autocomplete="off" required>
        </div>
         <div class="modal-footer">
          <input type="text" name="hidden_prod_id" id="hidden_prod_id">
          <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Save Changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
         </div>
        </form> 
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->