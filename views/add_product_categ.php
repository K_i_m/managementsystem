<!-- Content Wrapper. Contains page content -->
  <section class="content-header">
    <h1>
     Category
    </h1>
    <!-- <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol> -->
  </section>
 <!-- / Content Wrapper. Contains page content -->
 <br>
  <section class="content-header">
  	<div class="btn-group float-right">
  	  <button type="button" class="btn btn-primary" onclick="modal_add_product()"><span class="fa fa-plus"></span> Add</button>	
  	  <button type="button" class="btn btn-danger" id="btn_delete" onclick="delete_product()"><span class="fa fa-trash"></span> Delete</button>
  	</div>
  </section>
 <br><br>
 <!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Table</h3>
      </div>
      <div class="card-body">
        <table id="table_product" class="table table-bordered table-hover" style="width: 100%;">
          <thead>
            <tr>
              <th><input type="checkbox" value="0" onchange="checkAll(this)"></th>
              <th>#</th>
               <th></th>
              <th>Category</th>
            </tr>
          </thead> 
          <tbody>
          </tbody> 
        </table> 
      </div>    
    </div>
  </div>
</div>
	<?php require 'modals/add_product_modal.php'; ?>
	<?php require 'modals/update_product_modal.php'; ?>
</section>
<script type="text/javascript">
$(document).ready(function(){
	getProduct();
});

function delete_product(){
	$("#btn_delete").prop('disabled', true);
	$("#btn_delete").html('<span class="fa fa-spin fa-spinner"></span> Loading ....');
	var checkedValues = $('input:checkbox:checked').map(function() {
		return this.value;
	}).get();
	id = [];
	if(checkedValues == ""){
		warningAlert();
		$("#btn_delete").prop('disabled', false);
		$("#btn_delete").html('<span class="fa fa-trash"></span> Delete');
	}else{
		var retVal = confirm("Are you sure to delete?");
		if( retVal == true ){
			$.ajax({
			type:"POST",
			url:"ajax/delete_product_categ.php",
			data:{
				id:checkedValues
			},
			success:function(data){
				alert(data);
				getProduct();

				$("#btn_delete").prop('disabled', false);
				$("#btn_delete").html('<span class="fa fa-trash"></span> Delete');
			}
		});
		}else{

		}
	}
	
}

$("#frm_update_product").submit(function(e){
	e.preventDefault();

	$("#btn_update_categ").prop('disabled', true);
	$("#btn_update_categ").html('<span class="fa fa-spin fa-spinner"></span> Loading ....');

	$.ajax({
		type:"POST",
		url:"ajax/update_product.php",
		data:$(this).serialize(),
		success:function(data){
			if(data == 1){
				$("#update-product-modal").modal('hide');
				$("#table_product").DataTable().destroy();
				getProduct();
				success_update();	
			}else if(data == 2){
				alert("Product Already Exist !");
				$("#btn_update_categ").prop('disabled', false);
				$("#btn_update_categ").html('<span class="fa fa-check-circle"></span> Save Changes');
			}else{
				error_code();
			}

			$("#btn_update_categ").prop('disabled', false);
			$("#btn_update_categ").html('<span class="fa fa-check-circle"></span> Save Changes');
		}
	});

});

function modal_update_product(id){
	$("#hidden_prod_id").val(id);
	
	$.post("ajax/previewProduct.php",{
		prod_id:id
	}, function (data, status){
		var o = JSON.parse(data);

		$("#update_prod_name").val(o.prod_categ);

	}

	);

	$("#update-product-modal").modal('show');

	
}

function getProduct(){
	$("#table_product").DataTable().destroy();
	$("#table_product").dataTable({
		"processing":true,
		"ajax":{
			"type":"POST",
			"url":"ajax/datatables/product_data.php",
			"dataSrc":"data",			
		},
		"columns":[
			{
				"mRender":function(data, type, row){
					return "<input type='checkbox' value='" + row.prod_id +"' >";
				}
			},
			{
				"data":"count"
			},
			{
				"mRender":function(data, type, row){
					return "<center>"+

						 "<button type='button' class='btn btn-info btn-xs' onclick='modal_update_product("+row.prod_id+")'><span class='fa fa-edit'></span></button>";

					"</center>";
				}
			},
			{
				"data":"prod_name"
			}
		]
	});


}

function modal_add_product(){
	$("#add-product-modal").modal('show');
}

$("#frm_add_product").submit(function(e){
	e.preventDefault();

	$("#btn_add_categ").prop('disabled', true);
	$("#btn_add_categ").html('<span class="fa fa-spin fa-spinner"></span> Loading ....');

	$.ajax({
		type:"POST",
		url:"ajax/add_product.php",
		data:$(this).serialize(),
		success:function(data){
			if(data == 1){
				success_add();
				$("#frm_add_product")[0].reset();
				$("#add-product-modal").modal('hide');
				getProduct();
			}else if(data == 2){
				alert("Category already exist !");
				$("#btn_add_categ").prop('disabled', false);
				$("#btn_add_categ").html('<span class="fa fa-check-circle"></span> Save');
			}else{
				error_code();
			}

			$("#btn_add_categ").prop('disabled', false);
			$("#btn_add_categ").html('<span class="fa fa-check-circle"></span> Save');
		}
	});
});
</script>
