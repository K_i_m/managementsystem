<!-- Content Wrapper. Contains page content -->
  <section class="content-header">
    <h1>
      Product Entry
    </h1>
    <!-- <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol> -->
  </section>
 <!-- / Content Wrapper. Contains page content -->
 <br>
  <section class="content-header">
  	  <button type="button" class="btn btn-danger float-right" onclick="delete_product()"><span class="fa fa-trash"></span> Delete</button>
    <button type="button" class="btn btn-primary float-right" onclick="modal_add_product()"><span class="fa fa-plus"></span> Add</button>
  </section>
 <br><br>
 <!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Table</h3>
      </div>
      <div class="card-body">
        <table id="table_product" class="table table-bordered table-hover" style="width: 100%;">
          <thead>
            <tr>
              <th><input type="checkbox" value="0" onchange="checkAll(this)"></th>
              <th>#</th>
              <th></th>
              <th>Product Name</th>
              <th>Product Code</th>
              <th>Category</th>
              <th>Description</th>
              <th>Date Added</th>
            </tr>
          </thead> 
          <tbody>
          </tbody> 
        </table> 
      </div>    
    </div>
  </div>
</div>
</section>